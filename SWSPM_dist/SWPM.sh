#!/bin/csh
set noglob

set wl = 2048
set ws = 256
set enc = "COMPLEMENTS_TOGETHER"
# i /mnt/c/D/fiit/DP-ELKI/data/Mammals.csv
# o /mnt/c/D/fiit/DP-ELKI/data/out_mammals/

while($#argv)
	switch($argv[1])
		case -i:
			shift
			set in = $argv[1]
			breaksw
		case -o:
			shift
			set out = $argv[1]
			breaksw
		case -l:
			shift
			set wl = $argv[1]
			breaksw
		case -s:
			shift
			set ws = $argv[1]
			breaksw
		case -c:
			set enc = "COMPLEMENTS_CROSS"
			breaksw
		default:
			breaksw
	endsw
	shift
end


java -cp "./elki_nr.jar:./dependency/*" de.lmu.ifi.dbs.elki.application.KDDCLIApplication \
   	-dbc.in $in \
	-dbc.parser stuba.StringVectorParser \
	-parser.colsep ,\\s \
	-parser.vector-type StringVector_externalID \
	-dbc.filter stuba.DNA_ST_WindowFilter \
	-filter.dataProvider FastaDnaDataProvider \
	-filter.numerifier RootOfZero_DNA_Numerifier \
	-numerifier.encoding $enc \
	-filter.transform FFTTransformer \
	-filter.windowSize $wl \
	-filter.windowShift $ws \
	-algorithm clustering.hierarchical.SLINK,stuba.DistanceMatrixCreator \
	-algorithm.distancefunction minkowski.EuclideanDistanceFunction \
	-resulthandler stuba.ExportLabelledVisualizations,stuba.MatrixWriter,stuba.NewickTreeResultWriter \
	-vis.output $out \
	-rw.output $out/distmx.txt \
	-rw.output $out/newick.txt >& /dev/stdout
	
