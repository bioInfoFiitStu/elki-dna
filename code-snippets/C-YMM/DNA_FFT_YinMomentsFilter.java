package de.lmu.ifi.dbs.elki.datasource.filter.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.lmu.ifi.dbs.elki.algorithm.stuba.MemoryAwareTask;
import de.lmu.ifi.dbs.elki.data.DoubleVector;
import de.lmu.ifi.dbs.elki.data.FeatureVector;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.stuba.MultipleTypeVectorList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalMultipleID;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.math.stuba.impl.FFTbase;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;

/**
 * Filter to create DNA fourier power spectra 'moments' out of
 * StringVector_externalID description
 * http://dx.doi.org/10.1016/j.jtbi.2015.02.026
 * 
 * @author Tomas Farkas algo by Hoang, Yin et al
 *
 */
public class DNA_FFT_YinMomentsFilter extends AbstractDNAFilter implements MemoryAwareTask {

  protected DNA_FFT_YinMomentsFilter(DNA_DataProvider provider) {
    super(provider);
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);
    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));

    if(currentItem instanceof StringVector_externalMultipleID) {
      data[inx] = processTask((StringVector_externalMultipleID) currentItem);
    }
    else {
      data[inx] = processTask(currentItem);
    }
  }

  protected DoubleVector processTask(StringVector_externalID currentItem) {

    String fname = currentItem.getFileName();
    String[] params = currentItem.getParams();
    char[] dataString = provider.getData(fname, false, params);
    if(dataString != null) {
      double[] moments = getMoments(dataString);
      return new DoubleVector(moments);
    }
    else {
      LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams()));
      return new DoubleVector(new double[12]);
    }
  }

  @SuppressWarnings("rawtypes")
  protected MultipleTypeVectorList processTask(StringVector_externalMultipleID currentItem) {

    int fNum = currentItem.getNumberOfFiles();
    List<FeatureVector> result = new ArrayList<>();

    for(int f = 0; f < fNum; f++) {
      String fname = currentItem.getFileName(f);
      String[] params = currentItem.getParams(f);
      char[] dataString = provider.getData(fname, false, params);
      if(dataString != null) {
        double[] moments = getMoments(dataString);
        result.add(new DoubleVector(moments));
      }
      else {
        LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams(f)));
        result.add(new DoubleVector(new double[12]));
      }
    }
    return new MultipleTypeVectorList(result);
  }

  @Override
  protected void createArray_hook() {
    data = new DoubleVector[getNrOfTasks()];
  }

  @Override
  public long getCurrentMemoryUsage() {
    return 0;
  }

  protected double[] getMoments(char[] dataString) {
    double[] result = new double[12];
    int length = dataString.length;
    int fftLength = 1 << (int) (Math.log(length - 1) / Math.log(2) + 1);

    // for each of A,C,G,T
    for(int curBase = 0; curBase < 4; curBase++) {

      //// 1. compute BSI out of dataStrings
      int countOfTheBase = 0;
      float bsi[] = new float[fftLength];
      for(int i = 0; i < length; i++) {
        char curB = dataString[i];
        if(curB == curBase) {
          bsi[i] = 1;
          countOfTheBase++;
        }
      }
      //// 2. compute U = DFT(BSI)
      float[] im = new float[fftLength];
      FFTbase fft = new FFTbase(fftLength, false);
      fft.fft(bsi, im);
      //// 3. compute PS = U^2 //result now in bsi
      for(int i = 0; i < fftLength; i++) {
        bsi[i] = bsi[i] * bsi[i] + im[i] * im[i];
      }
      //// 4. compute moments
      for(int j = 1; j <= 3; j++) {
        double summ = 0;
        for(int k = 0; k < fftLength; k++) {
          summ += Math.pow(bsi[k], j);
        }
        double jThMoment = 1.0 / (Math.pow(countOfTheBase, (j - 1)) * Math.pow(length - countOfTheBase, (j - 1))) * summ;

        result[curBase + (j - 1) * 4] = jThMoment;
      }
    }
    return result;
  }
  
  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    if(data.getClass().getComponentType().equals(DoubleVector.class)){
      res.appendColumn(TypeUtil.DOUBLE_VECTOR_FIELD, Arrays.asList(data));
    }else{
      res.appendColumn(TypeUtil.MULTIPLE_TYPE_VECTOR, Arrays.asList(data));
    }
    return res;
  }

  public static class Parametrizer extends AbstractDNAFilter.Parameterizer {

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

    }

    @Override
    protected Object makeInstance() {
      return new DNA_FFT_YinMomentsFilter(provider);
    }

  }

}
