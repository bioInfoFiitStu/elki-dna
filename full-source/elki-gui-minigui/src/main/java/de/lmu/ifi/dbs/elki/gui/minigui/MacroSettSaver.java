package de.lmu.ifi.dbs.elki.gui.minigui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import de.lmu.ifi.dbs.elki.utilities.pairs.Pair;

/**
 * Lightweight and more powerful version of SavedSettingsFile
 * 
 * @author Tomas Farkas
 *
 */
public class MacroSettSaver {

	String dirname;
	File dir;

	public MacroSettSaver(String dirname) {
		this.dirname = dirname;
		this.dir = new File(dirname);
		if (!dir.exists()) {
			try {
				dir.mkdir();
			} catch (SecurityException se) {
				se.printStackTrace();
			}
		}
	}

	public void remove(String name) {
		File file = new File(dirname + "/" + name);
		file.delete();
	}

	public void save(String name, List<List<String>> params) {
		PrintWriter writer;
		try {
			File file = new File(dirname + "/" + name);
			writer = new PrintWriter(file);
			writer.print("");

			for (List<String> list : params) {
				for (String s : list) {
					writer.append("\"" + s + "\",");
				}
				writer.append("\n");
			}

			writer.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void save(String name, String value) {
		PrintWriter writer;
		try {
			File file = new File(dirname + "/" + name);
			writer = new PrintWriter(file);
			writer.print(value);

			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public int size() {
		return dir.list().length;
	}

	public String getNameAt(int index) {
		String names[] = dir.list();
		return names[index];
	}

	public String getContents(String fname) {
		String res = "";
		File f = new File(dirname + "/" + fname);
		if (f.exists() && !f.isDirectory()) {

			try {
				BufferedReader br = new BufferedReader(new FileReader(dirname + "/" + fname));
				String line = br.readLine();
				while (line != null) {
					res += line + "\n";
					line = br.readLine();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return res;
	}

	public Pair<String, List<List<String>>> getElementAt(int index) {
		String names[] = dir.list();
		String fname = names[index];
		List<List<String>> content = new ArrayList<List<String>>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(dirname + "/" + fname));
			String line = br.readLine();
			while (line != null) {
				content.add(parseLineEntry(line));
				line = br.readLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return new Pair<String, List<List<String>>>(fname, content);
	}

	public List<List<String>> parseCommands(String content) {
		List<List<String>> parsedContent = new ArrayList<List<String>>();
		String lines[] = content.split("\n");
		for (String command : lines) {
			parsedContent.add(parseLineEntry(command));
		}
		return parsedContent;
	}

	private List<String> parseLineEntry(String line) {
		List<String> res = new ArrayList<String>();

		String[] params = line.substring(1, line.length() - 2).split("\",\"");
		for (String param : params) {
			res.add(param);
		}
		return res;
	}
}
