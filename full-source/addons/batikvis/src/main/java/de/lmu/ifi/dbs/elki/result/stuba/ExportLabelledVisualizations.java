package de.lmu.ifi.dbs.elki.result.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.batik.util.SVGConstants;

import de.lmu.ifi.dbs.elki.database.Database;
import de.lmu.ifi.dbs.elki.database.ids.DBID;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.result.ExportVisualizations;
import de.lmu.ifi.dbs.elki.result.Result;
import de.lmu.ifi.dbs.elki.result.ResultHierarchy;
import de.lmu.ifi.dbs.elki.result.LabelEnhancedResultHandler;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.CommonConstraints;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.DoubleParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.FileParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.FileParameter.FileType;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;
import de.lmu.ifi.dbs.elki.visualization.VisualizationTask;
import de.lmu.ifi.dbs.elki.visualization.VisualizerParameterizer;
import de.lmu.ifi.dbs.elki.visualization.gui.VisualizationPlot;
import de.lmu.ifi.dbs.elki.visualization.gui.overview.PlotItem;
import de.lmu.ifi.dbs.elki.visualization.visualizers.Visualization;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.LabelEnhancedVisFactory;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.LabelProvider;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.RelationLabelProvider;

/**
 * 
 * @author Tomas Farkas
 *
 */
public class ExportLabelledVisualizations extends ExportVisualizations implements LabelEnhancedResultHandler {

  protected LabelProvider labeller;

  private static final Logging LOG = Logging.getLogger(ExportLabelledVisualizations.class);

  public ExportLabelledVisualizations(File output, VisualizerParameterizer manager, double ratio, LabelProvider labeller) {
    super(output, manager, ratio);
    this.labeller = labeller;
  }

  @Override
  public void processNewResult(ResultHierarchy hier, Result newResult) {

    if(newResult instanceof Database) {
      labeller.setDatabase((Database) newResult);
    }
    else {
      LOG.warning("Initial database lost");
    }
    processNewResultConcrete(hier, newResult);
  }

  @Override
  public String getLabel(DBID id) {
    return labeller.getLabel(id);
  }

  @Override
  protected void processItem(PlotItem item) {
    // Descend into subitems
    for(Iterator<PlotItem> iter = item.subitems.iterator(); iter.hasNext();) {
      processItem(iter.next());
    }
    if(item.taskSize() <= 0) {
      return;
    }
    item.sort();
    final double width = item.w, height = item.h;

    VisualizationPlot svgp = new VisualizationPlot();
    svgp.getRoot().setAttribute(SVGConstants.SVG_WIDTH_ATTRIBUTE, "20cm");
    svgp.getRoot().setAttribute(SVGConstants.SVG_HEIGHT_ATTRIBUTE, (20 * height / width) + "cm");
    svgp.getRoot().setAttribute(SVGConstants.SVG_VIEW_BOX_ATTRIBUTE, "0 0 " + width + " " + height);

    ArrayList<Visualization> layers = new ArrayList<>();
    for(Iterator<VisualizationTask> iter = item.tasks.iterator(); iter.hasNext();) {
      VisualizationTask task = iter.next();
      if(task.hasAnyFlags(VisualizationTask.FLAG_NO_DETAIL | VisualizationTask.FLAG_NO_EXPORT) || !task.visible) {
        continue;
      }
      try {
        Visualization v;
        if(task.getFactory() instanceof LabelEnhancedVisFactory) {
          v = ((LabelEnhancedVisFactory) task.getFactory()).makeVisualization(this, task, svgp, width, height, item.proj);
        }
        else {
          v = task.getFactory().makeVisualization(task, svgp, width, height, item.proj);
        }
        layers.add(v);
      }
      catch(Exception e) {
        if(Logging.getLogger(task.getFactory().getClass()).isDebugging()) {
          LOG.exception("Visualization failed.", e);
        }
        else {
          LOG.warning("Visualizer " + task.getFactory().getClass().getName() + " failed - enable debugging to see details.");
        }
      }
    }
    if(layers.isEmpty()) {
      return;
    }
    for(Visualization layer : layers) {
      if(layer.getLayer() == null) {
        LOG.warning("NULL layer seen.");
        continue;
      }
      svgp.getRoot().appendChild(layer.getLayer());
    }
    svgp.updateStyleElement();

    String prefix = null;
    prefix = (prefix == null && item.proj != null) ? item.proj.getMenuName() : prefix;
    prefix = (prefix == null && item.tasks.size() > 0) ? item.tasks.get(0).getMenuName() : prefix;
    prefix = (prefix != null ? prefix : "plot");
    // TODO: generate names...
    Integer count = counter.get(prefix);
    counter.put(prefix, count = count == null ? 1 : (count + 1));
    File outname = new File(output, prefix + "-" + count + ".svg");
    try {
      svgp.saveAsSVG(outname);
    }
    catch(Exception e) {
      LOG.warning("Export of visualization failed.", e);
    }
    for(Visualization layer : layers) {
      layer.destroy();
    }
  }

  public static class Parameterizer extends AbstractParameterizer {

    public static final OptionID RATIO_ID = new OptionID("vis.ratio", "The width/heigh ratio of the output.");

    public static final OptionID FOLDER_ID = new OptionID("vis.output", "The output folder.");

    VisualizerParameterizer manager;

    File output;

    double ratio;

    public static final OptionID LABEL_PROVIDER_ID = new OptionID("vis.labeller", "The class to provide labels for DB objects.");

    LabelProvider labeller;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);
      FileParameter outputP = new FileParameter(FOLDER_ID, FileType.OUTPUT_FILE);
      if(config.grab(outputP)) {
        output = outputP.getValue();
      }
      DoubleParameter ratioP = new DoubleParameter(RATIO_ID, 1.33);
      ratioP.addConstraint(CommonConstraints.GREATER_THAN_ZERO_DOUBLE);
      if(config.grab(ratioP)) {
        ratio = ratioP.doubleValue();
      }

      final ObjectParameter param = new ObjectParameter<>(LABEL_PROVIDER_ID, LabelProvider.class, RelationLabelProvider.class);
      if(config.grab(param)) {
        labeller = (LabelProvider) param.instantiateClass(config);
      }

      manager = config.tryInstantiate(VisualizerParameterizer.class);
    }

    protected ExportLabelledVisualizations makeInstance() {
      return new ExportLabelledVisualizations(output, manager, ratio, labeller);
    }
  }

}
