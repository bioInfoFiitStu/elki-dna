package de.lmu.ifi.dbs.elki.result.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import de.lmu.ifi.dbs.elki.algorithm.stuba.DistanceMatrixCreator;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.math.linearalgebra.Matrix;
import de.lmu.ifi.dbs.elki.result.Result;
import de.lmu.ifi.dbs.elki.result.ResultHandler;
import de.lmu.ifi.dbs.elki.result.ResultHierarchy;
import de.lmu.ifi.dbs.elki.utilities.datastructures.hierarchy.Hierarchy.Iter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.FileParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.FileParameter.FileType;

/**
 * 
 * @author Tomas Farkas
 *
 */
public class MatrixWriter implements ResultHandler {

  private static final Logging LOG = Logging.getLogger(DistanceMatrixCreator.class);

  File output;

  public MatrixWriter(File output) {
    this.output = output;
  }

  @Override
  public void processNewResult(ResultHierarchy hier, Result newResult) {

    MatrixResult res = null;
    for(Iter<Result> iter = hier.iterAll(); iter.valid(); iter.advance()) {
      if(iter.get().getClass() == MatrixResult.class) {
        res = (MatrixResult) iter.get();
        break;
      }
    }
    if(res == null) {
      LOG.warning("MatrixResult not found");
      return;
    }

    // -------------get data

    Matrix m = res.getMatrix();
    double[][] elements = m.getArrayRef();
    String[] labelsC = res.getLabelsC();
    String[] labelsR = res.getLabelsR();

    // -------------write matrix
    
    writeMatrix(elements, labelsC, labelsR, false);
    
  }

  protected void writeMatrix(double[][] elements, String[] labelsC, String[] labelsR, boolean toAppend) {

    try {

      if(!output.exists()) {
        output.createNewFile();
      }

      FileWriter fw = new FileWriter(output.getAbsoluteFile(), toAppend);
      BufferedWriter bw = new BufferedWriter(fw);

      bw.write("                           ");
      for(int j = 0; j < elements[0].length; j++) {
        String caption = ("|" + labelsC[j] + "              ").substring(0, 15);
        bw.write(caption + " ");
      }
      bw.write("\n");

      for(int i = 0; i < elements.length; i++) {
        String caption = (labelsR[i] + "                            ").substring(0, 25);
        bw.write(caption + " ");
        for(int j = 0; j < elements[i].length; j++) {
          bw.write("|" + String.format("%15.11f", elements[i][j]));
        }
        bw.write("\n");
      }

      bw.close();

    }
    catch(Exception e) {
      LOG.error("Write to file has failed");
    }

  }

  public static class Parameterizer extends AbstractParameterizer {

    public static final OptionID FOLDER_ID = new OptionID("rw.output", "The output folder.");

    File output;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);
      FileParameter outputP = new FileParameter(FOLDER_ID, FileType.OUTPUT_FILE);
      if(config.grab(outputP)) {
        output = outputP.getValue();
      }
    }

    protected MatrixWriter makeInstance() {
      return new MatrixWriter(output);
    }
  }

}
