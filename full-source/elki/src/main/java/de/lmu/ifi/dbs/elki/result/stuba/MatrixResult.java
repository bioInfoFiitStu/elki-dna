package de.lmu.ifi.dbs.elki.result.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import de.lmu.ifi.dbs.elki.result.Result;
import de.lmu.ifi.dbs.elki.math.linearalgebra.*;

/**
 * 
 * @author Tomas
 *
 */
public class MatrixResult implements Result {

  String[] labelsR;
  String[] labelsC;
  Matrix result;
  String name = "MatrixResult";
    
  public MatrixResult(Matrix result) {
    super();
    this.result = result;
    labelsR = new String[result.getRowDimensionality()];
    labelsC = new String[result.getColumnDimensionality()];
  }
  
  public MatrixResult(Matrix result, String name){
    super();
    this.result = result;
    this.name = name;
    labelsR = new String[result.getRowDimensionality()];
    labelsC = new String[result.getColumnDimensionality()];
  }
  
  public MatrixResult(Matrix result, String[] labelsR, String[] labelsC) {
    super();
    this.labelsR = labelsR;
    this.labelsC = labelsC;
    this.result = result;
  }
  
  public MatrixResult(Matrix result, String name, String[] labelsR, String[] labelsC){
    super();
    this.labelsR = labelsR;
    this.labelsC = labelsC;
    this.result = result;
    this.name = name;
  }

  @Override
  public String getLongName() {
    return name;
  }

  @Override
  public String getShortName() {
    return name;
  }  
  
  public String[] getLabelsR() {
    return labelsR;
  }

  public void setLabelsR(String[] labelsR) {
    this.labelsR = labelsR;
  }

  public String[] getLabelsC() {
    return labelsC;
  }

  public void setLabelsY(String[] labelsC) {
    this.labelsC = labelsC;
  }

  public Matrix getMatrix() {
    return result;
  }

  public void setMatrix(Matrix result) {
    this.result = result;
    if(labelsR==null){
      labelsR = new String[result.getRowDimensionality()];
    }
    if(labelsC==null){
      labelsC = new String[result.getColumnDimensionality()];
    }
    
  } 
  

}
