package de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import de.lmu.ifi.dbs.elki.utilities.pairs.Pair;

/**
 * 
 * DNADataProvider implementation that reads input Fasta file, finds position by
 * specified label a retrieves data
 * 
 * @author Tomas Farkas
 *
 */
public class FastaDnaDataProvider implements DNA_DataProvider {

  private static Map<String, Index> indices = new ConcurrentHashMap<>();

  @Override
  public char[] getData(String fname, boolean dense, String... params) {

    // 1st argument is pattern to be found in data header
    String tag = params[0];
    StringBuilder sequence = new StringBuilder();

    Index i = indices.get(fname);
    if(i == null) {
      i = new Index(fname);
      indices.put(fname, i);
    }
    // BufferedReader br;
    // try {
    // br = new BufferedReader(new FileReader(fname));
    // boolean getting = false;
    //
    // String line = br.readLine();
    // while(line != null) {
    // if(getting) {
    // if(line.equals("")) {
    // break;
    // }
    // sequence.append(line);
    // }
    // else {
    // if(line.indexOf(tag) != -1) {
    // getting = true;
    // }
    // }
    // line = br.readLine();
    // }
    //
    // br.close();
    //
    // }
    // catch(Exception e) {
    // e.printStackTrace();
    // }

    try {
      BufferedReader br = new BufferedReader(new FileReader(fname));
      Pair<Integer, Integer> poss = i.getPosition(tag);
      if(poss == null) {
        br.close();
        return null;
      }
      char[] buffer = new char[poss.second - poss.first];
      br.skip(poss.first);
      br.read(buffer, 0, poss.second - poss.first);
      sequence.append(buffer);
      
      br.close();
    }
    catch(IOException e) {
      e.printStackTrace();
    }
    if(dense) {
      return densify(numerify(sequence.toString().toCharArray()));
    }
    return numerify(sequence.toString().toCharArray());
  }

  private char[] numerify(char[] data) {

    for(int i = 0; i < data.length; i++) {
      switch(data[i]){
      case 'A':
      case 'a':
        data[i] = 0;
        break;
      case 'C':
      case 'c':
        data[i] = 1;
        break;
      case 'G':
      case 'g':
        data[i] = 2;
        break;
      case 'T':
      case 't':
        data[i] = 3;
        break;
      default:
        data[i] = 255;
      }
    }
    
    return data;

  }

  private char[] densify(char[] data) {
    char[] line = new char[data.length / 4];

    for(int i = 0; i < data.length / 4; i++) {
      line[i] = (char) ((data[(i << 2)] << 6) + (data[(i << 2) + 1] << 4) + (data[(i << 2) + 2] << 2) + (data[(i << 2) + 3]));
    }
    return line;

  }

  public static class Index {
    private Map<SubEqualString, Pair<Integer, Integer>> positions = new ConcurrentHashMap<>();

    public Index(String fname) {
      buildIndex(fname);
    }

    public Pair<Integer, Integer> getPosition(String expr) {
      SubEqualString query = new SubEqualString(expr);
      Pair<Integer, Integer> position = positions.get(query);

      return position;
    }

    public void buildIndex(String fname) {
      FileInputStream fis;
      BufferedReader br;
      try {
        File f = new File(fname);
        fis = new FileInputStream(f);

        br = new BufferedReader(new InputStreamReader(fis));
        int position = 0;

        String line = br.readLine();
        String prev = null;
        Integer prevP = 0;
        while(line != null) {
          position += line.length() + 2; // FIXME CR-LF is windows only
          if(line.indexOf(">") != -1) {
            if(prev != null) {
              positions.put(new SubEqualString(prev), new Pair<>(prevP, position - line.length() - 2));
            }
            prev = line;
            prevP = position;
            // positions.put(new SubEqualString(line), (int)
            // fis.getChannel().position());
          }
          line = br.readLine();
        }
        positions.put(new SubEqualString(prev), new Pair<Integer, Integer>(prevP, position));
        
        br.close();
        fis.close();
      }
      catch(IOException e) {
        e.printStackTrace();
      }   
      
    }    

  }

}
