package de.lmu.ifi.dbs.elki.datasource.filter.stuba.deprecated;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-UniversitĆ¤t MĆ¼nchen
 Lehr- und Forschungseinheit fĆ¼r Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;

import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.SparseFloatVector;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.filter.stuba.AbstractDNAFilter;
import de.lmu.ifi.dbs.elki.datasource.filter.stuba.AbstractDNAFilter.Parameterizer;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.math.stuba.impl.FFTbase;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.LessEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.DoubleParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;

/**
 * DEPRECATED - use ST_Filter instead
 * @author Tomas Farkas
 *
 */
public class DNA_FFT_Filter extends AbstractDNAFilter{
  private int maxSeqL;
  private double significantRatio;

  protected DNA_FFT_Filter(DNA_DataProvider provider, int maxSeqL, double significantRatio) {
    super(provider);
    this.maxSeqL = maxSeqL;
    this.significantRatio = significantRatio;
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);

    //prepare
    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));
    String fname = currentItem.getFileName();
    char[] dataString = provider.getData(fname, false, currentItem.getParams());
    int dimensionality = (int) (dataString.length * significantRatio);
    
    // get FFT
    int fftLength = 1 << (int) (Math.log(maxSeqL - 1) / Math.log(2) + 1);
    float[] indicatorsR = new float[fftLength];
    float[] indicatorsI = new float[fftLength];

    for(int i = 0; i < dataString.length; i++) {
      if(dataString[i] == 0) { // using only A=1
        indicatorsR[i] = 1;
      }     
    }
    FFTbase fft = new FFTbase(fftLength, false);
    fft.fft(indicatorsR, indicatorsI);
    //abs and normalize
    for(int i = 0; i < indicatorsR.length; i++) {
      indicatorsR[i] = (indicatorsR[i]*indicatorsR[i] + indicatorsI[i]*indicatorsI[i]) / dataString.length;
    }        
    // get most significant values
    float[] transformed_sorted = new float[fftLength];
    for(int i = 0; i < fftLength; i++) {
      transformed_sorted[i] = indicatorsR[i];
    }
    Arrays.sort(transformed_sorted);
    float jThQuantile = transformed_sorted[fftLength - dimensionality];

    // find number of equal as the quantile
    for(int i = fftLength - dimensionality - 1; transformed_sorted[i] == jThQuantile && i >= 0; i--) {
      dimensionality++;
    }
    float[] values = new float[dimensionality];
    int[] indices = new int[dimensionality];
    int ctr = 0;
    for(int i = 0; i < fftLength; i++) {
      if(Math.abs(indicatorsR[i]) >= jThQuantile) {
        values[ctr] = (indicatorsR[i]);
        indices[ctr++] = (i);
      }
    }
    // save result
    data[inx] = new SparseFloatVector(indices, values, dimensionality);
  }

  @Override
  protected void createArray_hook() {
    data = new SparseFloatVector[getNrOfTasks()];
  }

  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    res.appendColumn(TypeUtil.SPARSE_VECTOR_FIELD, Arrays.asList(data));
    return res;
  }

  public static class Parametrizer extends AbstractDNAFilter.Parameterizer {

    protected static final OptionID SIG_RATIO = new OptionID("filter.significantRatio", "Ratio for significat values, e.g. 0.1 means select top 10%");
    protected static final OptionID MAX_S_LEN = new OptionID("filter.maxSeqLen", "Maximum data length among dataset, bp.");

    
    private double sigRatio;
    private int maxSeqL;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      DoubleParameter sigP = new DoubleParameter(SIG_RATIO);
      sigP.addConstraint(new GreaterConstraint(0));
      sigP.addConstraint(new LessEqualConstraint(1));
      
      IntParameter slP = new IntParameter(MAX_S_LEN);
      slP.addConstraint(new GreaterConstraint(0));

      if(config.grab(sigP)) {
        sigRatio = sigP.getValue();
      }
      if(config.grab(slP)) {
        maxSeqL = slP.getValue();
      }
    }

    @Override
    protected Object makeInstance() {
      return new DNA_WHT_Filter(provider, maxSeqL, sigRatio);
    }

  }
}