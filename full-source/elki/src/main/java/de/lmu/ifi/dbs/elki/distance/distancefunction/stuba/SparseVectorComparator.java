package de.lmu.ifi.dbs.elki.distance.distancefunction.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import de.lmu.ifi.dbs.elki.data.SparseNumberVector;
import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.distance.distancefunction.AbstractPrimitiveDistanceFunction;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.EnumParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;

/**
 * Compare two sparse vectors in n + m
 * 
 * @author Tomas Farkas, inspired by Shoshan Cohen
 *
 */

public class SparseVectorComparator extends AbstractPrimitiveDistanceFunction<SparseNumberVector> {

  protected SparseCompareType compType;

  protected int tolerance;

  public SparseVectorComparator(SparseCompareType compType, int tolerance) {
    this.compType = compType;
    this.tolerance = tolerance;
  }

  @Override
  public SimpleTypeInformation<? super SparseNumberVector> getInputTypeRestriction() {
    return (SimpleTypeInformation<? super SparseNumberVector>) TypeUtil.SPARSE_VECTOR_FIELD;
  }

  @Override
  public double distance(SparseNumberVector o1, SparseNumberVector o2) {

    // no hook methods nor OO principles for sake of performance
    if(tolerance == 0) { // default n+m behaviour
      switch(this.compType){
      case SUBTRACTIVE__NONMATCH_IGNORANT:
        return subtractiveDist(o1, o2);
      case DIVISIVE__NONMATCH_IGNORANT:
        return divisiveDist(o1, o2);
      case NONMATCH_SENSITIVE:
        return nonMatchDist(o1, o2);
      default:
        return 0;
      }
    }
    return 0;
  }

  private double subtractiveDist(SparseNumberVector o1, SparseNumberVector o2) {
    int o1Iter = o1.iter();
    int o2Iter = o2.iter();
    double result = 0;

    while(o1.iterValid(o1Iter) && o2.iterValid(o2Iter)) {
      if(o1.iterDim(o1Iter) < o2.iterDim(o2Iter)) {
        o1Iter = o1.iterAdvance(o1Iter);
      }
      else if(o1.iterDim(o1Iter) > o2.iterDim(o2Iter)) {
        o2Iter = o2.iterAdvance(o2Iter);
      }
      else {
        double v1 = o1.iterDoubleValue(o1Iter);
        double v2 = o2.iterDoubleValue(o2Iter);
        if(0 == v1 && 0 == v2) {
          continue;
        }
        result += Math.abs(v1 - v2);
        o1Iter = o1.iterAdvance(o1Iter);
        o2Iter = o2.iterAdvance(o2Iter);
      }
    }
    return result;
  }

  private double divisiveDist(SparseNumberVector o1, SparseNumberVector o2) {
    int o1Iter = o1.iter();
    int o2Iter = o2.iter();
    double result = 0;

    while(o1.iterValid(o1Iter) && o2.iterValid(o2Iter)) {
      if(o1.iterDim(o1Iter) < o2.iterDim(o2Iter)) {
        o1Iter = o1.iterAdvance(o1Iter);
      }
      else if(o1.iterDim(o1Iter) > o2.iterDim(o2Iter)) {
        o2Iter = o2.iterAdvance(o2Iter);
      }
      else {
        double v1 = o1.iterDoubleValue(o1Iter);
        double v2 = o2.iterDoubleValue(o2Iter);
        if(0 == v1 && 0 == v2) {
          continue;
        }
        result += Math.abs(v1 - v2) / (Math.abs(v1) > Math.abs(v2) ? Math.abs(v1) : Math.abs(v2));
        o1Iter = o1.iterAdvance(o1Iter);
        o2Iter = o2.iterAdvance(o2Iter);
      }
    }
    return result;
  }

  private double nonMatchDist(SparseNumberVector o1, SparseNumberVector o2) {
    int o1Iter = o1.iter();
    int o2Iter = o2.iter();
    int matchCnt = 0;

    while(o1.iterValid(o1Iter) && o2.iterValid(o2Iter)) {
      if(o1.iterDim(o1Iter) < o2.iterDim(o2Iter)) {
        o1Iter = o1.iterAdvance(o1Iter);
      }
      else if(o1.iterDim(o1Iter) > o2.iterDim(o2Iter)) {
        o2Iter = o2.iterAdvance(o2Iter);
      }
      else {
        matchCnt++;
        o1Iter = o1.iterAdvance(o1Iter);
        o2Iter = o2.iterAdvance(o2Iter);
      }
    }
    double result = (double)matchCnt / (double)(o1.getDimensionality() > o2.getDimensionality() ? o2.getDimensionality() : o1.getDimensionality());
    return 1-result;
  }

  // private double divisiveDistTolerant(SparseNumberVector o1,
  // SparseNumberVector o2) {
  // // copy values for easier step up-downs
  // int[] o1indices = new int[o1.getDimensionality()];
  // double[] o1values = new double[o1.getDimensionality()];
  // int[] o2indices = new int[o2.getDimensionality()];
  // double[] o2values = new double[o2.getDimensionality()];
  // int ctr = 0;
  // for(int iter = o1.iter(); o1.iterValid(iter); iter = o1.iterAdvance(iter))
  // {
  // o1indices[ctr] = o1.iterDim(iter);
  // o1values[ctr++] = o1.iterDoubleValue(iter);
  // }
  // ctr = 0;
  // for(int iter = o2.iter(); o2.iterValid(iter); iter = o2.iterAdvance(iter))
  // {
  // o2indices[ctr] = o2.iterDim(iter);
  // o2values[ctr++] = o2.iterDoubleValue(iter);
  // }
  //
  // double result = 1.0;
  // int o1ctr = 0;
  // int o1s = o1indices.length;
  // int o2ctr = 0;
  // int o2s = o2indices.length;
  //
  // while(o1ctr < o1s && o2ctr < o2s) {
  // // approx match
  // for(int i = 1;; i++) {
  // int inx1 = o1indices[o1ctr];
  // double val1 = o1values[o1ctr];
  // if(o2ctr - i > 0) {
  // int inx2 = o2indices[o1ctr];
  // double val2 = o2values[o1ctr];
  // int inxDiff = Math.abs(inx1 - inx2);
  // if(inxDiff <= tolerance) {
  // result += (1.0 / (inxDiff + 1)) * (1.0 / (inxDiff + 1)) * Math.abs(val1 -
  // val2);
  // // TODO not working, hv not needed
  // }
  // }
  // else {
  // break;
  // }
  // }
  // // exact match + shift
  // if(o1indices[o1ctr] < o2indices[o2ctr]) {
  // o1ctr++;
  // }
  // else if(o1indices[o1ctr] > o2indices[o2ctr]) {
  // o2ctr++;
  // }
  // else {
  // double v1 = o1values[o1ctr];
  // double v2 = o2values[o2ctr];
  // result += v1 / v2;
  // o1ctr++;
  // o2ctr++;
  // }
  // }
  //
  // return result;
  // }

  public enum SparseCompareType {
    SUBTRACTIVE__NONMATCH_IGNORANT, DIVISIVE__NONMATCH_IGNORANT, NONMATCH_SENSITIVE
  }

  public static class Parameterizer<O> extends AbstractParameterizer {

    protected static final OptionID COMP_TYPE = new OptionID("dist.compType", "Comparing realization");

    protected SparseCompareType compType = null;

    protected static final OptionID TOLERANCE = new OptionID("dist.tolerance", "Absolute tolerance in index");

    protected int tolerance;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      final EnumParameter<SparseCompareType> param = new EnumParameter<>(COMP_TYPE, SparseCompareType.class, SparseCompareType.NONMATCH_SENSITIVE);
      if(config.grab(param)) {
        compType = param.getValue();
      }

      final IntParameter param2 = new IntParameter(TOLERANCE, 0);
      param2.addConstraint(new GreaterConstraint(0));
      if(config.grab(param2)) {
        tolerance = param2.getValue();
      }
    }

    @Override
    protected Object makeInstance() {
      return new SparseVectorComparator(compType, tolerance);
    }

  }
}
