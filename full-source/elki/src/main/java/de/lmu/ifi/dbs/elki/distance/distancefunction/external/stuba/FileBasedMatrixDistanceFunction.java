package de.lmu.ifi.dbs.elki.distance.distancefunction.external.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.SubEqualString;
import de.lmu.ifi.dbs.elki.distance.distancefunction.AbstractPrimitiveDistanceFunction;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.result.stuba.MatrixReader;
import de.lmu.ifi.dbs.elki.result.stuba.MatrixResult;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.FileParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.LabelProvider;
import de.lmu.ifi.dbs.elki.visualization.visualizers.stuba.RelationLabelProvider;

/**
 * 
 * @author Tomas Farkas
 *
 */
public class FileBasedMatrixDistanceFunction extends AbstractPrimitiveDistanceFunction<LabelList> {

  private static final Logging LOG = Logging.getLogger(FileBasedMatrixDistanceFunction.class);

  File matrixFile;

  Map<SubEqualString, Map<SubEqualString, Double>> distances = new HashMap<>();
  
  protected FileBasedMatrixDistanceFunction(){};
  
  public FileBasedMatrixDistanceFunction(File matrixfile) {
    this.matrixFile = matrixfile;

    buildTable();
  }

  private void buildTable() {
    MatrixResult distM = (new MatrixReader()).readMatrix(matrixFile);

    String[] labels = distM.getLabelsC();
    double[][] matrix = distM.getMatrix().getArrayRef();
    
    for(int i = 0; i < labels.length; i++) {
      Map<SubEqualString, Double> row = new HashMap<SubEqualString, Double>();
      for(int j = 0; j < labels.length; j++) {
        row.put(new SubEqualString(labels[j]), matrix[i][j]);
      }
      distances.put(new SubEqualString(labels[i]), row);

    }

  }

  @Override
  public double distance(LabelList entity1, LabelList entity2) {
        
    String l1 = entity1.toString();
    String l2 = entity2.toString();
    
    Map<SubEqualString, Double> row = distances.get(new SubEqualString(l1));
    if(row == null) {
      LOG.warning("No row entry found in matrix for label " + l1);
      return 0;
    }
    Double dist = row.get(new SubEqualString(l2));
    if(dist == null) {
      LOG.warning("No entry found in matrix for label " + l2);
      return 0;
    }
    return dist;
  }
  
  @Override
  public SimpleTypeInformation<? super LabelList> getInputTypeRestriction() {
    return (SimpleTypeInformation<LabelList>) TypeUtil.LABELLIST;
  }

  @Override
  public boolean isSymmetric() {
    return false;
  }

  @Override
  public boolean isMetric() {
    return true;
  }

  public static class Parameterizer extends AbstractParameterizer {

    public static final OptionID MATRIX_ID = new OptionID("distance.matrix", "The name of the file containing the distance matrix.");
    protected File matrixfile = null;
    
    public static final OptionID LABEL_PROVIDER_ID = new OptionID("distance.labeller", "The class to provide labels for DB objects.");
    LabelProvider labeller;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);
      final FileParameter MATRIX_PARAM = new FileParameter(MATRIX_ID, FileParameter.FileType.INPUT_FILE);
      if(config.grab(MATRIX_PARAM)) {
        matrixfile = MATRIX_PARAM.getValue();
      }
      
      
      final ObjectParameter<LabelProvider> param = new ObjectParameter<>(LABEL_PROVIDER_ID, LabelProvider.class, RelationLabelProvider.class);
      if(config.grab(param)) {
        labeller = (LabelProvider) param.instantiateClass(config);
      }
    }

    @Override
    protected FileBasedMatrixDistanceFunction makeInstance() {
      return new FileBasedMatrixDistanceFunction(matrixfile);
    }
  }  

}
