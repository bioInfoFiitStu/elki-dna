package de.lmu.ifi.dbs.elki.datasource.filter.stuba.deprecated;

import java.util.Arrays;

import de.lmu.ifi.dbs.elki.algorithm.stuba.MemoryAwareTask;
import de.lmu.ifi.dbs.elki.data.FloatVector;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.filter.stuba.AbstractDNAFilter;
import de.lmu.ifi.dbs.elki.datasource.filter.stuba.AbstractDNAFilter.Parameterizer;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.math.stuba.impl.FFTbase;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * DNAFourierFuncion as filter implementations Creates DNA power spectra vector
 * out of data specified by StringVector_externalID
 * 
 * @author Tomas Farkas, algo by Hoang, Yin et al
 */
public class DNA_FFT_YinFilter extends AbstractDNAFilter implements MemoryAwareTask {

  protected int maxSeqLen;

  protected DNA_FFT_YinFilter(DNA_DataProvider provider, int maxSeqLen) {
    super(provider);
    this.maxSeqLen = maxSeqLen;
  }

  @Override
  protected void createArray_hook() {
    data = new FloatVector[getNrOfTasks()];
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);

    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));
    String fname = currentItem.getFileName();
    String[] params = currentItem.getParams();
    // get data
    char[] dataString = provider.getData(fname, false, params);
    if(dataString != null) {
      // get spectrum
      float[] spectrum = getSpectra(dataString);
      // normalize (not written explicitly in paper but absolutely crucial)
      for(int i = 0; i < spectrum.length; i++) {
        spectrum[i] = spectrum[i] / dataString.length;
      }
      data[inx] = new FloatVector(spectrum);
    }
    else {
      data[inx] = new FloatVector(new float[0]);
      LOG.warning("could not find data for identifier: "+Arrays.toString(currentItem.getParams()));

    }

  }

  @Override
  public long getCurrentMemoryUsage() {
    return 0;
  }

  private float[] getSpectra(char[] data) {

    byte selBase = 0; // lets compute by 'A's

    int length = data.length;
    // nearest higher power of 2
    int fftLength = 1 << (int) (Math.log(maxSeqLen - 1) / Math.log(2) + 1);

    float bsi[] = new float[fftLength];
    float valueOfMatch = (float) maxSeqLen / (float) length;
    for(int i = 0; i < length; i++) {
      char curB = data[i];
      if(curB == selBase) {
        bsi[i] = valueOfMatch;
      }
    }
    float[] im = new float[fftLength];
    FFTbase fft = new FFTbase(fftLength, false);
    fft.fft(bsi, im);

    for(int i = 0; i < fftLength; i++) {
      bsi[i] = bsi[i] * bsi[i] + im[i] * im[i];
    }

    return bsi;
  }

  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    res.appendColumn(TypeUtil.FLOAT_VECTOR_FIELD, Arrays.asList(data));
    return res;
  }

  public static class Parameterizer extends AbstractDNAFilter.Parameterizer {

    protected static final OptionID LONGEST_SEQ = new OptionID("dist.maxSeqLen", "Length of the lopngest sequence in the database, needed for ELKI architectural reasons.");

    int maxSeqLen = 1;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      IntParameter param = new IntParameter(LONGEST_SEQ, 1);
      param.addConstraint(new GreaterEqualConstraint(1));
      if(config.grab(param)) {
        maxSeqLen = param.getValue();
      }
    }

    @Override
    protected Object makeInstance() {
      return new DNA_FFT_YinFilter(provider, maxSeqLen);
    }
  }

}
