package de.lmu.ifi.dbs.elki.data.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.utilities.datastructures.arraylike.ArrayAdapter;
import de.lmu.ifi.dbs.elki.utilities.io.ByteBufferSerializer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;

/**
 * 
 * @author Tomas Farkas
 *
 */
public class StringVector_externalMultipleID extends StringVector_externalID {

  private static final Logging LOG = Logging.getLogger(StringVector_externalMultipleID.class);  
  public static final StringVector_externalMultipleID.Factory STATIC = new StringVector_externalMultipleID.Factory();
  public static final ByteBufferSerializer<StringVector_externalMultipleID> VARIABLE_SERIALIZER = new StringSerializer();

  List<String> fnameList = new ArrayList<>();
  List<String[]> paramsList = new ArrayList<>();
  
  protected StringVector_externalMultipleID(String[] values, boolean nocopy) {
    super(values, nocopy);
    build();
  }

  public StringVector_externalMultipleID(String[] values) {
    super(values);
    if(values.length<2){
      LOG.error("StringVector_externalID not containing mandatory <label, filename>");
    }
    build();
  }
  
  private void build(){    
    List<String> params = new ArrayList<>();
    for(int i=values.length-1; i>0; i--) {
      String s = values[i];
      
      if(s.startsWith("-f=")){
        fnameList.add(s.substring(3));
        Collections.reverse(params);
        paramsList.add(params.toArray(new String[0]));
        params = new ArrayList<>();
      }else{
        params.add(s);
      }
    }
    Collections.reverse(fnameList);
    Collections.reverse(paramsList);
  }
  
  /////////////////////////////////////////////

  public int getNumberOfFiles(){
    return fnameList.size();
  }
 
  public String getFileName(int inx) {
    return fnameList.get(inx);
  }

  public String[] getParams(int inx) {
    return paramsList.get(inx);
  }
  
  @Deprecated
  @Override
  public String getFileName() {
    LOG.warning("using incompatibile filter -> only first entity description used");
    return this.getValue(1).substring(3);
  }

  @Deprecated
  @Override
  public String[] getParams() {
    LOG.warning("using incompatibile filter -> only first entity description used");
    return new String[]{this.getValue(2)};
  }

  /**
   * 
   * @author Tomas Farkas
   *
   */
  public static class Parameterizer extends AbstractParameterizer {
    @Override
    protected StringVector_externalMultipleID.Factory makeInstance() {
      return STATIC;
    }
  }

  /**
   * 
   * @author Tomas Farkas
   *
   */
  public static class Factory implements StringVector.Factory<StringVector_externalMultipleID> {

    @Override
    public ByteBufferSerializer<StringVector_externalMultipleID> getDefaultSerializer() {
      return new StringSerializer();// VARIABLE_SERIALIZER;
    }

    @Override
    public Class<StringVector_externalMultipleID> getRestrictionClass() {
      return StringVector_externalMultipleID.class;
    }

    @Override
    public <A> StringVector_externalMultipleID newFeatureVector(A array, ArrayAdapter<? extends String, A> adapter) {
      int dim = adapter.size(array);
      String[] values = new String[dim];
      for(int i = 0; i < dim; i++) {
        values[i] = adapter.get(array, i);
      }
      return new StringVector_externalMultipleID(values, true);
    }

    @Override
    public StringVector_externalMultipleID newStringVector(String[] values) {
      return new StringVector_externalMultipleID(values);
    }

    @Override
    public StringVector_externalMultipleID newStringVector(List<String> values) {
      String[] array = values.toArray(new String[0]);
      return new StringVector_externalMultipleID(array);
    }

    @Override
    public StringVector_externalMultipleID newStringVector(StringVector values) {
      return new StringVector_externalMultipleID(values.toArray());
    }
  }

  /**
   * @author Tomas Farkas
   */
  public static class StringSerializer implements ByteBufferSerializer<StringVector_externalMultipleID> {

    @Override
    public StringVector_externalMultipleID fromByteBuffer(ByteBuffer buffer) throws IOException {
      String data = buffer.toString();
      String[] values = data.split(">delim<");
      return new StringVector_externalMultipleID(values, true);
    }

    @Override
    public void toByteBuffer(ByteBuffer buffer, StringVector_externalMultipleID vec) throws IOException {
      for(String s : vec.values) {
        buffer.put(s.getBytes());
        buffer.put(">delim<".getBytes());
      }
    }

    @Override
    public int getByteSize(StringVector_externalMultipleID vec) {
      int count = 0;
      for(String s : vec.values) {
        count += s.length() + 7;
      }
      return count;
    }
  }
}
