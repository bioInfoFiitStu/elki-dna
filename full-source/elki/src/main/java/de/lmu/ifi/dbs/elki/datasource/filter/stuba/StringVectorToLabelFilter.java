package de.lmu.ifi.dbs.elki.datasource.filter.stuba;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.filter.ObjectFilter;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Extracts a new relation of LabelList out of MultipleObjectsBundle's StringVector_externalID
 * @author Tomas Farkas.
 */
public class StringVectorToLabelFilter implements ObjectFilter{

  @SuppressWarnings("unchecked")
  @Override
  public MultipleObjectsBundle filter(MultipleObjectsBundle objects) {
 
    List<StringVector_externalID> idEntries = null;    
    for(int r = 0; r < objects.metaLength(); r++) {
      final SimpleTypeInformation<?> type = objects.meta(r);
      final List<?> column = objects.getColumn(r);
      if(!TypeUtil.STRING_VECTOR_EXTERNAL.isAssignableFromType(type)) {
        continue;
      }      
      idEntries = ((List<StringVector_externalID>) column);
    }
    
    List<LabelList> labels = new ArrayList<LabelList>();  
    
    for(StringVector_externalID idEntry : idEntries) {
      labels.add(LabelList.make(Arrays.asList(idEntry.getLabel()))); //harakiri kvoli tomu, ze konstruktor je private (neviem naco)
    }
    objects.appendColumn(TypeUtil.LABELLIST, labels);
    
    return objects;    
  }

}
